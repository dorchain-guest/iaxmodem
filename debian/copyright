Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Lee Howard <faxguy@howardsilvan.com>
Source: http://iaxmodem.sf.net
Files-Excluded: lib/spandsp/src/spandsp/mmx.h lib/spandsp/src/msvc lib/spandsp/libspandsp.vcproj lib/spandsp/autom4te.cache lib/libiax2/autom4te.cache
  lib/spandsp/INSTALL lib/spandsp/*Makefile.in
  lib/spandsp/aclocal.m4 lib/spandsp/config-h.in
  lib/spandsp/config/depcomp lib/spandsp/config/install-sh
  lib/spandsp/config/ltmain.sh lib/spandsp/config/missing
  lib/spandsp/configure 
  lib/libiax2/configure
  lib/libiax2/Makefile.in lib/libiax2/aclocal.m4
  lib/libiax2/ltmain.sh lib/libiax2/src/Makefile.in
Comment: Old copyright content
 This package was debianized by Julien BLACHE <jblache@debian.org> for
 Linbox (http://www.linbox.com/en) on Mon, 27 Feb 2006 19:33:17 +0100.
 .
 It was downloaded from: http://iaxmodem.sf.net
 .
 IAXmodem uses modified copies of libiax2 and spandsp.
 .
 IAXmodem
 --------
 Copyright Holder: Lee Howard <faxguy@howardsilvan.com>
 .
 License:
 .
 IAXmodem is released under the term of the GNU General Public License,
 version 2 or above. On Debian systems, the complete text of the license can
 be found in the /usr/share/common-licenses/GPL-2 file.
 .
 The following files have their own copyright statements and license:
   + compat/daemon.c:
     /*
      * Copyright (c) 2007 Albert Lee <trisk@acm.jhu.edu>.
      *
      * Permission is hereby granted, free of charge, to any person
      * obtaining a copy of this software and associated documentation
      * files (the "Software"), to deal in the Software without
      * restriction, including without limitation the rights to use,
      * copy, modify, merge, publish, distribute, sublicense, and/or sell
      * copies of the Software, and to permit persons to whom the
      * Software is furnished to do so, subject to the following
      * conditions:
      *
      * The above copyright notice and this permission notice shall be
      * included in all copies or substantial portions of the Software.
      *
      * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
      * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
      * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
      * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
      * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
      * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
      * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
      * OTHER DEALINGS IN THE SOFTWARE.
      */
 .
   + compat/timers.c:
      Copyright (C) 1991-1994,1996-2002,2003 Free Software Foundation, Inc.
        This file is part of the GNU C Library.
 . 
    This file is released under the terms of the GNU Lesser General Public
    License, version 2.1 or later. On Debian systems, the complete text of
    the license can be found in the /usr/share/common-licenses/LGPL-2 file.
 .
 .
 libiax2
 -------
 Copyright Holder: Mark Spencer <markster@linux-support.net>
 .
 License from the README file:
 .
 libiax: An implementation of the Inter-Asterisk eXchange protocol distributed
 under the terms of the GNU Lesser General Public License
 .
 The following files are marked as being released under the LGPL:
   src/frame.h
   src/iax-client.c
   src/iax.c
   src/iax.h
   src/iax2-parser.c
   src/iax2-parser.h
   src/iax2.h
   src/jitterbuf.c
   src/jitterbuf.h
   src/winpoop.h
 .
 The following files are marked as being released under the GPL:
   src/answer.h
   src/miniphone.c
   src/options.c
   src/options.h
   src/ring10.h
   src/winiphone.c
 .
 The following files are in the public domain:
   src/md5.c
   src/md5.h
 .
 On Debian systems, the complete text of the licenses can be found in the
 /usr/share/common-licenses/LGPL-2 and /usr/share/common-licenses/GPL-2 files.
 .
 .
 spandsp
 -------
 Copyright Holder: Steve Underwood <steveu@coppice.org>
 .
 License:
 .
 spandsp is released under the term of the GNU General Public License,
 version 2 or above. On Debian systems, the complete text of the license can
 be found in the /usr/share/common-licenses/GPL-2 file.

Files: *
Copyright: Lee Howard <faxguy@howardsilvan.com>
License: GPL-2.0-or-later

Files: compat/daemon.c:
Copyright: 2007 Albert Lee <trisk@acm.jhu.edu>
License: Explicit
     /*
      * Copyright (c) 2007 Albert Lee <trisk@acm.jhu.edu>.
      *
      * Permission is hereby granted, free of charge, to any person
      * obtaining a copy of this software and associated documentation
      * files (the "Software"), to deal in the Software without
      * restriction, including without limitation the rights to use,
      * copy, modify, merge, publish, distribute, sublicense, and/or sell
      * copies of the Software, and to permit persons to whom the
      * Software is furnished to do so, subject to the following
      * conditions:
      *
      * The above copyright notice and this permission notice shall be
      * included in all copies or substantial portions of the Software.
      *
      * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
      * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
      * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
      * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
      * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
      * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
      * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
      * OTHER DEALINGS IN THE SOFTWARE.
      */

Files: compat/timers.c:
Copyright: 1991-1994,1996-2002,2003 Free Software Foundation, Inc.
License: LGPL-2.1-or-later
      Copyright (C) 1991-1994,1996-2002,2003 Free Software Foundation, Inc.
        This file is part of the GNU C Library.
 . 
    This file is released under the terms of the GNU Lesser General Public
    License, version 2.1 or later. On Debian systems, the complete text of
    the license can be found in the /usr/share/common-licenses/LGPL-2 file.

Files: lib/libiax2/src/frame.h lib/libiax2/src/iax-client.c lib/libiax2/src/iax.c lib/libiax2/src/iax.h lib/libiax2/src/iax2-parser.c lib/libiax2/src/iax2-parser.h lib/libiax2/src/iax2.h lib/libiax2/src/jitterbuf.c lib/libiax2/src/jitterbuf.h lib/libiax2/src/winpoop.h
Copyright: Mark Spencer <markster@linux-support.net>
License: LGPL-2
 -------
 Copyright Holder: Mark Spencer <markster@linux-support.net>
 .
 License from the README file:
 .
 libiax: An implementation of the Inter-Asterisk eXchange protocol distributed
 under the terms of the GNU Lesser General Public License
 .
 On Debian systems, the complete text of the licenses can be found in the
 /usr/share/common-licenses/LGPL-2 file.

Files: lib/libiax2/src/answer.h lib/libiax2/src/miniphone.c lib/libiax2/src/options.c lib/libiax2/src/options.h lib/libiax2/src/ring10.h lib/libiax2/src/winiphone.c
Copyright: Mark Spencer <markster@linux-support.net>
License: GPL-2
 On Debian systems, the complete text of the licenses can be found in the
 /usr/share/common-licenses/GPL-2 file.

Files: lib/libiax2/src/md5.c lib/libiax2/src/md5.h
Copyright: Mark Spencer <markster@linux-support.net>
License: public-domain
 This code is in the public domain; do with it what you wish.

Files: lib/spandsp/*
Copyright: Steve Underwood <steveu@coppice.org>
License: GPL-2.0-or-later

License: GPL-2.0-or-later
 This license refers to the term of the GNU General Public License,
 version 2 or above. On Debian systems, the complete text of the license can
 be found in the /usr/share/common-licenses/GPL-2 file.
